Open Street Map - Chile
=======================

This is the source code of the Open Street Map Chilean comunity.

How to contribute
-----------------

 * Fork the project.
 * Make your feature addition or bug fix.
 * Send me a pull request. Bonus points for topic branches.

Copyright
---------

Copyright (c) 2010 Digitales por Chile ( digitalesporchile.org )

Dual licensed under the MIT and GPL licenses.
Check LICENCE-MIT and LICENSE-GPL for details.
